﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace RendeloAlkalmazas
{
    /// <summary>
    /// Interaction logic for EditWindow.xaml
    /// </summary>
    public partial class EditWindow : Window
    {
        public Dolgozo CurrentDolgozo
        {
            get { return (Resources["VM"] as EditWindowViewModel).CurrentDolgozo; }
        }

        public EditWindow()
        {
            InitializeComponent();

        }
        public void SetPlayerToEdit(Dolgozo p)
        {
            (Resources["VM"] as EditWindowViewModel).CurrentDolgozo = p;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }
    }
}
