﻿using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RendeloAlkalmazas
{
    class EditWindowViewModel : ViewModelBase
    {
        Dolgozo currentDolgozo;

        public Dolgozo CurrentDolgozo
        {
            get { return currentDolgozo; }
            set { Set(ref currentDolgozo, value); }
        }
        public EditWindowViewModel()
        {
            currentDolgozo = new Dolgozo();
        }
    }
}
