﻿using CommonServiceLocator;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace RendeloAlkalmazas
{
    class MainWindowViewModell : ViewModelBase
    {
        public ObservableCollection<Dolgozo> Dolgozok { get; set; }
        private Dolgozo dolgozoSelected;
        public Dolgozo DolgozoSelected
        {
            get { return dolgozoSelected; }
            set { Set(ref dolgozoSelected, value); }
        }
        public IDolgozoLogic Logic
        {
            get { return ServiceLocator.Current.GetInstance<IDolgozoLogic>(); }
        }
        public ICommand DeleteDolgozoCommand { get; private set; }
        public ICommand NewDolgozoCommand { get; private set; }
        public ICommand ModDolgozoCommand { get; private set; }
        public MainWindowViewModell()
        {
            Dolgozok = new ObservableCollection<Dolgozo>();
            DeleteDolgozoCommand = new RelayCommand(() => Logic.DeleteDolgozo(Dolgozok, DolgozoSelected), () => DolgozoSelected != null);
            NewDolgozoCommand = new RelayCommand(() => Logic.NewDolgozo(Dolgozok));
            ModDolgozoCommand = new RelayCommand(() => Logic.ModDolgozo(DolgozoSelected), () => DolgozoSelected != null);

            Dolgozok.Add(new Dolgozo("henrik", 1226455, new DateTime(1994, 10, 28), 5, CsomagEnum.Medium));
            Dolgozok.Add(new Dolgozo("henrik", 1226455, new DateTime(1994, 10, 28), 5, CsomagEnum.Medium));
            Dolgozok.Add(new Dolgozo("henrik", 1226455, new DateTime(1994, 10, 28), 5, CsomagEnum.Medium));
            Dolgozok.Add(new Dolgozo("henrik", 1226455, new DateTime(1994, 10, 28), 5, CsomagEnum.Medium));
            Dolgozok.Add(new Dolgozo("henrik", 1226455, new DateTime(1994, 10, 28), 5, CsomagEnum.Medium));
        }
    }
}
