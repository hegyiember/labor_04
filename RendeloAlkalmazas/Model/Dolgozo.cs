﻿using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RendeloAlkalmazas
{
    public class Dolgozo : ObservableObject
    {
        private string nev;
        private int bankszamlaszam;
        private DateTime datum; // public DateTime(int year, int month, int day);
        private int honapok;
        private CsomagEnum internetcsoomag;

        public Dolgozo(string nev, int bankszamlaszam, DateTime datum, int honapok, CsomagEnum internetcsoomag)
        {
            Nev = nev;
            Bankszamlaszam = bankszamlaszam;
            Datum = datum;
            Honapok = honapok;
            Internetcsoomag = internetcsoomag;
        }

        public Dolgozo()
        {

        }

        public Dolgozo CreateClone()
        {
            //shallow copy, de itt nem gond, mivel minden adattagunk érték típus(ként viselkedik)
            return this.MemberwiseClone() as Dolgozo;
        }

        public void CopyDataFromClone(Dolgozo clone)
        {
            this.Nev = clone.Nev;
            this.Bankszamlaszam = clone.Bankszamlaszam;
            this.Datum = clone.Datum;
            this.Honapok = clone.Honapok;
            this.Internetcsoomag = clone.Internetcsoomag;
        }
        public string Nev
        {
            get { return nev; }
            set { Set(ref nev, value); }
        }
        public int Bankszamlaszam
        {
            get { return bankszamlaszam; }
            set { Set(ref bankszamlaszam, value); }
        }
        public DateTime Datum
        {
            get { return datum; }
            set { Set(ref datum, value); }
        }
        public int Honapok
        {
            get { return honapok; }
            set { Set(ref honapok, value); }
        }
        internal CsomagEnum Internetcsoomag
        {
            get { return internetcsoomag; }
            set { Set(ref internetcsoomag, value); }
        }
    }
}
