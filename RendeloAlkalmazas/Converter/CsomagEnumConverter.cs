﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace RendeloAlkalmazas
{
    class CsomagEnumConverter : IValueConverter
    {
        /// <summary>
        /// CsomagEnum --> string
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            switch ((CsomagEnum)value)
            {
                case CsomagEnum.Slow: return "Slow";
                case CsomagEnum.Medium: return "Medium";
                case CsomagEnum.Fats: return "Fast";
                case CsomagEnum.Superfast: return "Superfast";
                default: return "HIBA!";
            }
        }

        /// <summary>
        /// string --y CsomagEnum
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            switch ((string)value)
            {
                case "Slow": return CsomagEnum.Slow;
                case "Medium": return CsomagEnum.Medium;
                case "Fast": return CsomagEnum.Fats;
                case "Superfast": return CsomagEnum.Superfast;
                default: return CsomagEnum.HIBA;
            }
        }
    }
}
