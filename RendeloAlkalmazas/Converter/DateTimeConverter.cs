﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace RendeloAlkalmazas
{
    class DateTimeConverter : IValueConverter
    {
        /// <summary>
        /// DateTime --> string
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            DateTime temp = (DateTime)value;
            return temp.Year + "/" + temp.Month + "/" + temp.Day;
        }

        /// <summary>
        /// string --> DateTime
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string[] temp = ((string)value).Split('/');
            return new DateTime(Int32.Parse(temp[0]), Int32.Parse(temp[1]), Int32.Parse(temp[2]));
        }
    }
}
