﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RendeloAlkalmazas
{
    interface IEditLogic
    {
        Dolgozo NewDolgozo();
        bool EditDolgozo(Dolgozo player);
    }
}
