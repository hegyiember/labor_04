﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RendeloAlkalmazas
{
    interface IDolgozoLogic
    {
        void DeleteDolgozo(IList<Dolgozo> csapat, Dolgozo dolgozo);
        void NewDolgozo(IList<Dolgozo> csapat);
        void ModDolgozo(Dolgozo dolgozo);
    }
}
