﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RendeloAlkalmazas
{
    class EditLogic : IEditLogic
    {
        public bool EditDolgozo(Dolgozo player)
        {
            EditWindow editWindow = new EditWindow();
            editWindow.SetPlayerToEdit(player);
            if (editWindow.ShowDialog() == true)
            {
                return true;
            }
            return false;
        }

        public Dolgozo NewDolgozo()
        {
            EditWindow editWindow = new EditWindow();
            if (editWindow.ShowDialog() == true)
            {
                return editWindow.CurrentDolgozo;
            }
            return null;
        }
    }
}
