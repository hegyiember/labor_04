﻿using CommonServiceLocator;
using GalaSoft.MvvmLight.Messaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RendeloAlkalmazas
{
    class DolgozoLogic : IDolgozoLogic
    {
        public void DeleteDolgozo(IList<Dolgozo> csapat, Dolgozo dolgozo)
        {
            csapat.Remove(dolgozo);
        }

        public void ModDolgozo(Dolgozo dolgozo)
        {
            Dolgozo clone = dolgozo.CreateClone();
            IEditLogic service = ServiceLocator.Current.GetInstance<IEditLogic>();
            if (service.EditDolgozo(clone) == true)
            {
                dolgozo.CopyDataFromClone(clone);
                Messenger.Default.Send("EDIT OK", "LogicResult");
            }
            else
            {
                Messenger.Default.Send("EDIT CANCEL", "LogicResult");
            }
        }

        public void NewDolgozo(IList<Dolgozo> csapat)
        {
            IEditLogic service = ServiceLocator.Current.GetInstance<IEditLogic>();
            Dolgozo p = service.NewDolgozo();
            if (p != null)
            {
                csapat.Add(p);
                Messenger.Default.Send("New player added", "LogicResult");
            }
            else
            {
                Messenger.Default.Send("Player not added", "LogicResult");
            }
        }
    }
}
